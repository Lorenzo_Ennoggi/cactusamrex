#include <fixmath.hxx> // include this before <cctk.h>
#include <cctk.h>
#include <cctk_Arguments_Checked.h>

namespace Z4c {

extern "C" void Z4c_Sync1(CCTK_ARGUMENTS) { DECLARE_CCTK_ARGUMENTS_Z4c_Sync1; }

} // namespace Z4c
